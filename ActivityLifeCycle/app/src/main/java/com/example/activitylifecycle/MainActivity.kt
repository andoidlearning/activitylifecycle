package com.example.activitylifecycle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i("LifeCycle","onCreate Calling")
    }

    override fun onStart() {
        super.onStart()

        Log.i("LifeCycle","onStart Calling")
    }

    override fun onResume() {
        super.onResume()

        Log.i("LifeCycle","onResume Calling")
    }
    
    override fun onPause() {
        super.onPause()
        Log.i("LifeCycle","onPause Calling")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("LifeCycle","onDestroy Calling")
    }
}